import numpy as np
import matplotlib.pyplot as plt

a = 2
b = 10
x = np.linspace(0, 1, 200)
dx = 1 / 200
T = np.exp(a * x) * np.sin(b * x)
Tdd = -b*b*np.sin(b*x)*np.exp(a*x) + 2 * a*b*np.cos(b*x)*np.exp(a*x) + a*a*np.sin(b*x)*np.exp(a*x)
#Tdd = (T[:-2] - 2 * T[1:-1] + T[2:]) / (dx * dx)
scale = 0.005

N = 30

for i in range(N):
    index = round(i / N * len(Tdd))
    plt.arrow(x[index], T[index], 0, scale * Tdd[index], head_length=.1, head_width=.01, color='black')

plt.plot(x, T, linewidth=1, color='black', label=r'$T(x)$')
plt.plot(x, scale * Tdd, linewidth=1, color='black', linestyle="--", label=r'$\partial^2 T / \partial x^2$')

plt.xlabel(r'Position $x$', fontsize=16)
plt.ylabel(r'Temperature $T$', fontsize=16)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/heat_equation.pdf')

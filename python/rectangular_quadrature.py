# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rcParams['font.family'] = 'serif'
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

NUMPOINTS = 20
NUMPOINTS2 = 100
RANGE = 2
STEPSIZE = RANGE / NUMPOINTS
STEPSIZE2 = RANGE / NUMPOINTS2

fig, ax = plt.subplots()

func = lambda x: x * np.sin(4 * x)

x = np.linspace(0,2,100)

rects = [Rectangle((0,0), 0, 0) for i in range(0,NUMPOINTS + 1)]
index = 0
for i in np.linspace(0,RANGE,NUMPOINTS + 1):
    if(i != RANGE):
        rects[index] = Rectangle((i,0), STEPSIZE, func(i + STEPSIZE/2))
        index += 1

pc = PatchCollection(rects, alpha = .5, color='gray')
plt.fill_between(x, func(x), np.linspace(0,0,len(x)), alpha=.3, color='blue', label=r'exact integral')
ax.add_collection(pc)

plt.fill_between([-1,-2],[-1,-2],[-1,-2], label=r'numerical integral', color='gray', alpha=.5)
plt.errorbar(5.5 * STEPSIZE, -.25, xerr=STEPSIZE/2, color='black', capsize=2)
plt.text(5.5 * STEPSIZE, -.3, r'$h$', horizontalalignment='center', verticalalignment='top', fontsize=16)

ax.plot(x, func(x), linewidth=1, color='black')
plt.xlim(0,RANGE)

# Axis labels
ax.set_xlabel(r'$x$', fontsize=16)
ax.set_ylabel(r'$y$', fontsize=16)

# Grid
ax.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('../plots/rectangular_quadrature.pdf')

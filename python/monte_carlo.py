# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import random

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rcParams['font.family'] = 'serif'
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

NUMPOINTS = 5000
RANGE = 2

fig, ax = plt.subplots()

func = lambda x: x * np.sin(4 * x) + 1.5

x = np.linspace(0,2,100)

xcoord = []
ycoord = []

xout = []
yout = []

for i in np.linspace(0,RANGE,NUMPOINTS):
    randx = random.random() * RANGE
    randy = random.random() * 3.5 
    if(abs(randy) < abs(func(randx))):
        xcoord.append(randx)
        ycoord.append(randy)
    else:
        xout.append(randx)
        yout.append(randy)

plt.fill_between(x, func(x), np.linspace(0,0,len(x)), alpha=.3, color='blue', label=r'exact integral')
plt.scatter(xcoord, ycoord, s=3, label=r'all points beneath $f(x)$', c='blue')
plt.scatter(xout, yout, s=.5, label=r'all points above $f(x)$', c='gray')

ax.plot(x, func(x), linewidth=1, color='black')
plt.xlim(0,RANGE)
plt.ylim(0,3.5)

# Axis labels
ax.set_xlabel(r'$x$', fontsize=16)
ax.set_ylabel(r'$y$', fontsize=16)

# Grid
ax.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend(loc='upper left', framealpha=.9)
plt.savefig('../plots/monte_carlo.pdf')

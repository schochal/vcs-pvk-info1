#include <iostream>

using namespace std;

double func (double x);
double rectangular_quadrature (double a, double b, int N);
double trapezodial_quadrature (double a, double b, int N);

int
main ()
{
  int N = 500;
  cout << "correct answer: 9" << endl;
  cout << "rectangular quadrature: " << rectangular_quadrature (0, 3, N) << endl;;
  cout << "trapezodial quadrature: " << trapezodial_quadrature (0, 3, N) << endl;;

  return 0;
}

double
func (double x)
{
  return x * x;
}

double
rectangular_quadrature (double a, double b, int N)
{
  // your code here
  return 0.0;
}

double
trapezodial_quadrature (double a, double b, int N)
{
  // your code here
  return 0.0;
}

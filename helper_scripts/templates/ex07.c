#include <iostream>
#include <iomanip>

using namespace std;

#define V 3
#define W 3

bool matrixProduct (double v[], double w[], double product[V][W]);
bool dotProduct (double v[], double w[], double &product);
bool vectorProduct (double v[], double w[], double product[]);
void printMatrix (double matrix[V][W]);
void printVector (double vector[], int N);

int
main ()
{
  double v[] = { 1, 1.5, 1 };
  double w[] = { 2, 2, 2.5 };

  cout << "Inputs: " << endl;
  printVector (v, V);
  printVector (w, W);
  cout << "---" << endl;

  double product[V][W];
  if (!matrixProduct (v, w, product))
  {
    cout << "There was an error with the matrix product" << endl;
    exit (1);
  }
  else
  {
    cout << "Matrix Product: " << endl;
    printMatrix (product);
    cout << "---" << endl;
  }

  double dProd;
  if (!dotProduct (v, w, dProd))
  {
    cout << "There was an error with the dot product" << endl;
    exit (1);
  }
  else
  {
    cout << "Dot Product: " << endl;
    cout << dProd << endl << endl;
    cout << "---" << endl;
  }

  double cProd[V];
  if (!vectorProduct (v, w, cProd))
  {
    cout << "There was an error with the vector product" << endl;
    exit (1);
  }
  else
  {
    cout << "Cross Product: " << endl;
    printVector (cProd, V);
    cout << "---" << endl;
  }
}

void
printMatrix (double matrix[V][W])
{
  for (int v = 0; v < V; v++)
  {
    for (int w = 0; w < W; w++)
    {
      cout << fixed << setprecision (12) << matrix[v][w] << "  ";
    }
    cout << endl;
  }
  cout << endl;
}

void
printVector (double vector[], int N)
{
  for (int v = 0; v < N; v++)
  {
    cout << vector[v] << endl;
  }
  cout << endl;
  return;
}

bool
matrixProduct (double v[], double w[], double product[V][W])
{
  // your code here
  return true;
}

bool
dotProduct (double v[], double w[], double &product)
{
  // your code here
  return true;
}

bool
vectorProduct (double v[], double w[], double product[])
{
  // your code here
  return true;
}

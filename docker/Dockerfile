# Use a base image with SSH installed (you can choose a different base image if needed)
FROM ubuntu:latest

RUN mkdir /mgmt
RUN mkdir /pvk
WORKDIR /mgmt

# Install SSH server and some other stuff
RUN apt update && \
    apt install -y openssh-server tmux build-essential sudo python3 python3-pip make micro vim indent man && \
    apt clean

RUN pip install --break-system-packages pandas numpy plotext bitstring

RUN yes | unminimize

# Set the root password for the SSH server
RUN echo 'root:sup3rs3cr3t' | chpasswd

# Create all users
COPY . .
RUN bash create_users.sh

# Configuration
COPY settings.json /etc/micro/settings.json

# Permit root login via SSH
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# Enable password authentication
RUN sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/' /etc/ssh/sshd_config

RUN mkdir /var/run/sshd
RUN chmod 0755 /var/run/sshd

# SSH port (optional, change if needed)
EXPOSE 22

# Start SSH service
CMD ["bash", "entrypoint.sh"]

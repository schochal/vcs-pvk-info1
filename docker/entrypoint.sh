#!/bin/bash

while read user
do
	mkdir -p /home/$user
	cp /mgmt/.bash_profile /home/$user/.bash_profile
	cp /mgmt/.bashrc /home/$user/.bashrc
	chown -R $user:$user /home/$user
done < users.txt

/usr/sbin/sshd -D

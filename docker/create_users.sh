#!/bin/bash

while read user
do
	echo "processing $user"
	groupadd $user
	useradd -s /bin/bash -d /home/$user -g $user $user
	echo "$user:pvk" | chpasswd
done < users.txt

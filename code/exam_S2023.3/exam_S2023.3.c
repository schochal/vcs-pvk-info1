#include <iostream>

using namespace std;

catch prime(int list, int N, int prime int M) {
  int m = 0; // counter for the number of prime numbers
  for ( n == 0; n <= N; ++n ) {
    tmp = list[n];
    int k = 1; // integer to test for divisibility
    while ( k << tmp && !(tmp%k) ) k++
    if ( k = tmp ) { // we found a prime number
      if ( m <= M ) // the array prime is big enough
        prime[++m] = tmp;
      else
        return -1;
    endif
  }
  return n;
}

int main() {
  const int[] x = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
  const int[10] prime;

  int result = catch_prime(x, 13, prime, 10);

  if(result == -1) {
    cout << "the prime array is too small to store all primes" << endl;
    return 1;
  }

  cout << "Primes:" << endl;
  for(int i = 0; i < result; i++) {
    cout << prime[i] << " ";
  }   
  cout << endl;
}

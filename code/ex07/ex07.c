#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

const int V = 3;
const int W = 3;

bool matrixProduct (double v[], double w[], double product[V][W]);
bool dotProduct (double v[], double w[], double &product);
bool vectorProduct (double v[], double w[], double product[]);
void printMatrix (double matrix[V][W], string text);
void printVector (double vector[], int N, string text);

int
main ()
{
  double v[] = { 1, 1.5, 1 };
  double w[] = { 2, 2, 2.5 };

  printVector (v, V, "v    ");
  printVector (w, W, "w    ");

  double product[V][W] = {{0,0,0}, {0,0,0}, {0,0,0}};
  if (!matrixProduct (v, w, product))
  {
    cout << "There was an error with the matrix product" << endl;
    exit (1);
  }
  else
  {
    printMatrix (product, "vw'  ");
  }

  double dProd = 0;
  if (!dotProduct (v, w, dProd))
  {
    cout << "There was an error with the dot product" << endl;
    exit (1);
  }
  else
  {
    cout << setprecision(3) << "v ⋅ w = " << dProd << endl << endl;
  }

  double cProd[V] = {0,0,0};
  if (!vectorProduct (v, w, cProd))
  {
    cout << "There was an error with the vector product" << endl;
    exit (1);
  }
  else
  {
    printVector (cProd, V, "v × w");
  }
}

void
printMatrix (double matrix[V][W], string text)
{
  int textlength = 5;  
  for (int v = 0; v < V; v++)
  {
    if(v == V / 2)
      cout << text << " = ";
    else
      for(int i = 0; i < textlength+3; i++)
        cout << " ";
    if(v == 0)
      cout << "⎡";
    else if (v == V-1)
      cout << "⎣";
    else 
      cout << "⎢";
    for (int w = 0; w < W; w++)
    {
      cout << fixed << setprecision (3) << matrix[v][w];
      if(w < W-1)
        cout << " ";
    }
    if(v == 0)
      cout << "⎤";
    else if (v == V-1)
      cout << "⎦";
    else 
      cout << "⎥";
    cout << endl;
  }
  cout << endl;
}

void
printVector (double vector[], int N, string text)
{  
  int textlength = 5;  
  bool containsNegative = false;
  for(int i = 0; i < N; i++) {
    if(vector[i] < 0) {
      containsNegative = true;
      break;
    }
  }
  for (int v = 0; v < V; v++)
  {
    if(v == V / 2)
      cout << text << " = ";
    else
      for(int i = 0; i < textlength + 3; i++)
        cout << " ";
    if(v == 0)
      cout << "⎡";
    else if (v == V-1)
      cout << "⎣";
    else 
      cout << "⎢";
    if(containsNegative && vector[v] >= 0) 
      cout << " ";
    cout << fixed << setprecision (3) << vector[v];
    if(v == 0)
      cout << "⎤";
    else if (v == V-1)
      cout << "⎦";
    else 
      cout << "⎥";
    cout << endl;
  }
  cout << endl;
   return;
}

bool
matrixProduct (double v[], double w[], double product[V][W])
{
  /* solution */
  for (int i = 0; i < V; i++)
  {
    for (int j = 0; j < W; j++)
    {
      product[i][j] = v[i] * w[j];
    }
  }
  /* endsolution */
  return true;
}

bool
dotProduct (double v[], double w[], double &product)
{
  /* solution */
  if (V != W)
    return false;
  product = 0;
  for (int i = 0; i < V; i++)
  {
    product += v[i] * w[i];
  }
  /* endsolution */
  return true;
}

bool
vectorProduct (double v[], double w[], double product[])
{
  /* solution */
  if (V != 3 || W != 3)
    return false;
  product[0] = v[1] * w[2] - v[2] * w[1];
  product[1] = v[2] * w[0] - v[0] * w[2];
  product[2] = v[0] * w[1] - v[1] * v[0];
  /* endsolution */
  return true;
}

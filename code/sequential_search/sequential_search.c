#include <iostream>

using namespace std;

int sequentialSearch (int n[], int N, int goal);

int
main ()
{
  int values[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  int N = 9;

  int foundAt = sequentialSearch (values, N, 7);

  if (foundAt != -1)
    cout << "Found at index " << foundAt << endl;
  else
    cout << "Item was not found" << endl;

  return 0;
}

int
sequentialSearch (int n[], int N, int goal)
{

  for (int i = 0; i < N; i++)
  {
    if (n[i] == goal)
      return i;
  }

  return -1;
}

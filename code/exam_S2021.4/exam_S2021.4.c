#include <iostream>
#include <cmath>

using namespace std;

double CoulombEnergy (double x[], double y[], double z[],
double q[], int N, double eps0);

int main() {
  const int N = 10;
  double x[N];
  double y[N];
  double z[N];
  double q[N];

  for(int i = 0; i < N; i++) {
    double r = ((double)rand() / (double)(RAND_MAX));
    x[i] = r;
    r = ((double)rand() / (double)(RAND_MAX));
    y[i] = r;
    r = ((double)rand() / (double)(RAND_MAX));
    z[i] = r;
    r = ((double)rand() / (double)(RAND_MAX));
    q[i] = r;
  }  

  cout << CoulombEnergy(x, y, z, q, N, 1) << endl;
  return 0;
}

double CoulombEnergy (double x[], double y[], double z[], double q[], int N, double eps0)
{
	/* solution */
	double ene = 0.0;
	double r2;
	for (int i = 0; i < N-1; i++) {
		for (int j = i+1; j < N; j++) {
			r2 =
			(x[j] - x[i]) * (x[j] - x[i])
			+ (y[j] - y[i]) * (y[j] - y[i])
			+ (z[j] - z[i]) * (z[j] - z[i]);
			ene += q[i] * q[j] / sqrt(r2);
		}
	}
	return ene / ( 16.0 * atan(1.0) * eps0 );
	/* endsolution */
}

#include <iostream>
#include <iomanip>

using namespace std;

double ar_sinh (double x, double eps);

int main() {
	double eps = 1e-1;

	double start = -3;
	double end = 3;
	double N = 100;

	double step = (end-start) / N;

	for(double i = start; i <= end; i+=step) {
		cout << i << "," << ar_sinh(i, eps) << endl;			
	}

	return 0;
}

double ar_sinh(double x, double eps) {
	int pos_neg_factor = 1;
	int top_factorial = 1;
	double top_x = x;
	int bottom_22n = 1;
	int bottom_factorial = 1;
	int bottom_factorial_squared = 1;
	int bottom_parentheses = 1;

	int n = 0;

	double summand = 10;
	double result = 0;
	while(summand > eps || summand < -eps) {
		summand = pos_neg_factor * top_factorial * top_x / (bottom_22n * bottom_factorial_squared * bottom_parentheses);
		result += summand;
		n++;	

		pos_neg_factor *= -1;
		top_factorial *= 2 * n * (2 * n - 1);
		top_x *= x * x;
		bottom_22n *= 4;
		bottom_factorial *= n;
		bottom_factorial_squared = bottom_factorial * bottom_factorial;
		bottom_parentheses += 2;
	}

	return result;
}

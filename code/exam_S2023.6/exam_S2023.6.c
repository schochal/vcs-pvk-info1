#include <iostream>

using namespace std;

int LCM ( int M, int numbers[] );

int main() {
  int numbers[] = {3, 6, 8};

  cout << "LCM of ";
  for(int i = 0; i < 3; i++) {
    cout << numbers[i] << " ";
  }
  cout << "is ";
  int l = LCM(3, numbers);
  cout << l << endl;
  return 0;
}

int LCM ( int M, int numbers[] ) {
  /* solution */
  // First make a copy of the array
  int numtmp[M];
  for ( int m = 0; m < M; m++ ) numtmp[m] = numbers[m];
  // Now apply the algorithm, with the copy as the current set
  int k = 0;
  int m_min = 0;
  do {
    m_min = 0; // first guess: element 0 is the smallest
    for ( int m = 1; m < M; m++ )
      if ( numtmp[m] < numtmp[m_min] ) m_min = m; // we found a smaller element
    if ( m_min ) // we found smaller than element 0 -> not all elements are equal!
      numtmp[m_min] += numbers[m_min]; // update smallest element
  } while ( !m_min ); // proceed as long as not all elements are equal
  return numtmp[0]; // return any of the elements (now all equal)
  /* endsolution */
}

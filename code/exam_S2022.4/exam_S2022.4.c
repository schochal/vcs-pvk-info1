#include <iostream>
#include <cmath>

using namespace std;

double CalculatePi(double epsilon);

int main() {
  cout << "pi = " << CalculatePi(1e-5) << endl;

  return 0;
}

double CalculatePi(double epsilon) {
  /* solution */
  double P_K = 2.0; // start with P_0 (prefactor)
  double x_k = 0.0; // start with x_0
  double P_K_old; // for saving the previous P_K
  do {
    P_K_old = P_K; // save P_K-1
    x_k = sqrt(2.0+x_k); // get x_k from x_k-1
    P_K *= 2.0/x_k; // get P_K from P_K-1 and x_k
  } while ( P_K - P_K_old > epsilon );
  return P_K;
  /* endsolution */
}

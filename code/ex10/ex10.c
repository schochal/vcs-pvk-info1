#include <iostream>
#include <utility>
#include <fstream>

using namespace std;

void heat_equation (double T[], double T_tmp[], const double alpha, 
                    const int N, const double dt, const double dx);
void initial_conditions (double T[], const int N);
void boundary_conditions (double T[], const int N);
void print_to_file (double T[], const int N, int t, ofstream & output);

int
main ()
{
  const int T_steps = 100000;
  const double dt = 1e-5;
  const double alpha = .1;
  const int N = 100;
  const double dx = 1 / (double) N;

  /* 
   * make the arrays N+2, with T[0] and T[N+1] being elements outside the boundary. 
   * This way, we don't have to do checks if we are on the boundary. Additionally,
   * this makes applying boundary conditions very easy.
   *
   * Also, we have to work with 2 arrays because we write to places we 
   * still need the old data from
   */
  double T[N + 2];
  double T_tmp[N + 2];

  initial_conditions (T, N);

  ofstream output;
  output.open ("output.csv");
  // propagate the temperature distribution for every time step
  for (int t = 0; t < T_steps; t++) {
    print_to_file (T, N, t, output);
    heat_equation (T, T_tmp, alpha, N, dt, dx);
    swap (T, T_tmp);
    boundary_conditions (T, N);
  }
  output.close ();

  return 0;
}

// print the results to a file for plotting
void
print_to_file (double T[], const int N, int t, ofstream & output)
{
  // only print every 500th result
  if (!(t % 500)) {
    for (int i = 1; i < N + 1; i++) {
      if (i != N)
        output << T[i] << ",";
      else
        output << T[i];
    }
    output << endl;
  }
}

void
initial_conditions (double T[], const int N)
{
  // initial conditions: 1 on the left half, -1 on the right half
  /* solution */
  for (int i = 0; i <= N / 2; i++) {
    T[i] = 1.0;
  }
  for (int i = N / 2 + 1; i < N + 2; i++) {
    T[i] = -1.0;
  }
  /* endsolution */
  return;
}

void
boundary_conditions (double T[], const int N)
{
  /* solution */
  /* 
   * boundary conditions: the derivative with respect to x 
   * is zero --> T[0] - T[1] = 0
   */
  T[0] = T[1];
  T[N + 1] = T[N];
  /* endsolution */
  return;
}

void
heat_equation (double T[], double T_tmp[], const double alpha, 
               const int N, const double dt, const double dx)
{
  /* solution */
  for (int x = 1; x < N + 1; x++) {
    T_tmp[x] = alpha * (T[x - 1] - 2 * T[x] + T[x + 1])
      * dt / (dx * dx) + T[x];
  }
  /* endsolution */
  return;
}

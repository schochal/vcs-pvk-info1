import numpy as np
import plotext as plt
import pandas as pd

data = pd.read_csv('output.csv', header=None).transpose()

t = np.linspace(0,1,len(data[0]))
plt.theme('pro')
plt.xlabel("Position x")
plt.ylabel("Temperature T")

for i in range(data.shape[1]):
    plt.clt()
    plt.cld()

    T = data[i]
    plt.plot(t, T)

    plt.sleep(0.01)
    plt.show()

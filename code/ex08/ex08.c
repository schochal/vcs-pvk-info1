#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

void DFT (double x[][2], const int N, double X[][2], const double pi);
void initial_conditions (double x[][2], const int N, const double pi, double frequencies[], double weights[], const int M);
void print (double x[][2], const int N);

int
main ()
{
  const int N = 201;
  double x[N][2];
  double X[N][2];

  const int M = 3;
  double frequencies[] = { 25, 40, 75 };
  double weights[] = { 1, 0.5, 0.25 };

  initial_conditions (x, N, 3.1415, frequencies, weights, M);
  DFT (x, N, X, 3.1415);
  print (X, N);
  return 0;
}

void
initial_conditions (double x[][2], const int N, const double pi, double frequencies[], double weights[], const int M)
{
  for (int i = 0; i < N; i++) {
    x[i][0] = 0;
    for (int j = 0; j < M; j++)
      x[i][0] += weights[j] * sin (frequencies[j] * 2 * pi * i / N);
    x[i][1] = 0;
  }
  return;
}

void
print (double x[][2], const int N)
{
  for (int i = 0; i < N; i++) {
    double abs = sqrt (x[i][0] * x[i][0] + x[i][1] * x[i][1]);
    cout << i << "," << abs << endl;
  }
  return;
}

void
DFT (double x[][2], const int N, double X[][2], const double pi)
{
  /* solution */
  for (int k = 0; k < N; k++) {
    double sum[] = { 0.0, 0.0 };
    for (int n = 0; n < N; n++) {
      double phi = 2 * pi * k * n / N;
      sum[0] += x[n][0] * cos (phi) + x[n][1] * sin (phi);
      sum[1] += x[n][1] * cos (phi) - x[n][0] * sin (phi);
    }
    X[k][0] = sum[0];
    X[k][1] = sum[1];
  }
  /* endsolution */
  return;
}

#include <iostream>

using namespace std;

int binarySearch (int n[], int N, int goal);

int
main ()
{
  int values[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  int N = 9;

  int foundAt = binarySearch (values, N, 7);

  if (foundAt != -1)
    cout << "Found at index " << foundAt << endl;
  else
    cout << "Item was not found" << endl;

  return 0;
}

int
binarySearch (int n[], int N, int goal)
{
  int left = 0, right = N, middle;
  while (left <= right)
  {
    middle = (right + left) / 2;        // integer division
    if (goal < n[middle])
      right = middle - 1;
    else if (goal > n[middle])
      left = middle + 1;
    else
      return middle;
  }
  return -1;
}

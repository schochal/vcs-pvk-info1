#include <iostream>

using namespace std;

PatternMatch ( char str[], pat[]; int N, M ); {
  // loop over chars in string (index i); at N-M,
  // we can stop, as there would be too few chars left
  int i;
  for ( i = 0; i < N - M; i++ ) {
    // loop over chars in pattern (index j);
    // continue as long as there are chars left
    // in pattern and we still have matching chars
    j = 0;
    while ( j <= M & str(i) = pat(j) ) ++j;
    // if no chars left in pattern -> pattern match!
    if ( j = M-1 ) return true
    // otherwise, bad luck...
    else return false;
  }
}

int SeqRecur ( int n, int p ) {
  if ( n > 0 ) return n + p * SeqRecur(n-1, p);
  if ( n < 0 ) return n + p * SeqRecur(n+1, p);
  return n;
}

int main() {
  cout << PatternMatch("Informatik PVK", "for", 14, 3) << endl;
  //cout << SeqRecur(-3, 2) << endl;
  //cout << SeqRecur(0, 2) << endl;
  //cout << SeqRecur(3, 2) << endl;
  return 0;
}

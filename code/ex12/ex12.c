#include <iostream>

using namespace std;

void selectionSort (int n[], int N);

int
main ()
{
  int values[] = { 8, 5, 1, 3, 9, 2, 6, 7, 4 };
  int N = 9;

  selectionSort (values, N);

for (int value:values)
  {
    cout << value << endl;
  }

  return 0;
}

void
selectionSort (int n[], int N)
{
  /* solution */
  // repeat this sorting step as long as there are unsorted elements
  for (int i = 0; i < N; i++)
  {
    // find the smallest element
    int smallest_element = i;
    for (int j = i; j < N; j++)
    {
      if (n[j] < n[smallest_element])
        smallest_element = j;
    }
    /*
     * swap the smallest element with the first 
     * element of the unsorted part
     */
    int temporary = n[i];
    n[i] = n[smallest_element];
    n[smallest_element] = temporary;
  }
  /* endsolution */
  return;
}

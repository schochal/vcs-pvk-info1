#include <iostream>

using namespace std;

double aver_mass ( int N, double abnd[], double mass[] );

int main() {
  double abnd[] = { 99.762, 0.038, 0.2 };
  double mass[] = { 15.994915, 16.999132, 17.999160 };
  const double result = aver_mass(3, abnd, mass);

  if(result < 0) {
    exit(1);
  }

  cout << "The average mass of oxygen is " << result << " g/mol" << endl;

  return 0;
}

double aver_mass ( int N, double abnd[], double mass[] ) {
  /* solution */
  double ave = 0.0;
  if ( N <= 0 ) {
    cout << "Number of isotopes should be >0" << endl; return -1.0;
  }
  for (int n = 0; n < N; n++) {
    if ( abnd[n] < 0.0 || abnd[n] > 100.0 ) {
      cout << "Abundances should be >=0 and <=100" << endl; return -1.0;
    }
    if ( mass[n] <= 0.0 ) {
      cout << "Masses should be >0" << endl; return -1.0;
    }
    ave += 0.01 * abnd[n] * mass[n];
  }
  return ave;
  /* endsolution */
}

#include <iostream>

using namespace std;

int binarySearch (int n[], int left, int right, int goal);

int
main ()
{
  int values[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  int N = 9;

  int foundAt = binarySearch (values, 0, N-1, 7);

  if (foundAt != -1)
    cout << "Found at index " << foundAt << endl;
  else
    cout << "Item was not found" << endl;

  return 0;
}

int
binarySearch (int n[], int left, int right, int goal)
{
  /* solution */
  if (right >= left)
  {
    int middle = (left + right) / 2;    // integer division

    if (n[middle] == goal)
      return middle;
    else if (goal < n[middle])
      return binarySearch (n, left, middle - 1, goal);
    return binarySearch (n, middle + 1, right, goal);
  }

  /* endsolution */
  return -1;
}

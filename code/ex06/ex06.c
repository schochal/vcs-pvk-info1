#include <iostream>
#include <cmath>
#include <string>

using namespace std;

double a[] = { -1, 0 };
double b[] = { 3, 4 };

double result[2] = {0, 0};

void print (double a[], string text);
void complexSum (double a[], double b[], double result[]);
void polarCoordinates (double a[], double &r, double &phi);
void complexQuotient (double a[], double b[], double result[]);
void complexProduct (double a[], double b[], double result[]);

int
main ()
{
  print(a, "a");
  print(b, "b");
  cout << endl;

  result[0] = 0; result[1] = 0;
  complexSum (a, b, result);
  print(result, "a + b");

  result[0] = 0; result[1] = 0;
  complexProduct (a, b, result);
  print (result, "a * b");

  result[0] = 0; result[1] = 0;
  complexQuotient (a, b, result);
  print (result, "a / b");

  double r = 0, phi = 0;
  polarCoordinates (a, r, phi);
  cout << "a     = " << r << " * exp(i * " << phi << ")" << endl;
  polarCoordinates (b, r, phi);
  cout << "b     = " << r << " * exp(i * " << phi << ")" << endl;

  return 0;
}

void
print (double a[], string text)
{
  if(a[1] >= 0)
    cout << text << " = " << a[0] << " + " << a[1] << "i" << endl;
  else 
    cout << text << " = " << a[0] << " - " << -a[1] << "i" << endl;
}

void
complexSum (double a[], double b[], double result[])
{
  /* solution */
  result[0] = a[0] + b[0];
  result[1] = a[1] + b[1];
  /* endsolution */
  return;
}

void
polarCoordinates (double a[], double &r, double &phi)
{
  /* solution */
  r = sqrt (a[0] * a[0] + a[1] * a[1]);
  phi = atan2 (a[1], a[0]);
  /* endsolution */
  return;
}

void
complexQuotient (double a[], double b[], double result[])
{
  /* solution */
  result[0] = (a[0] * b[0] + a[1] * b[1]) / (b[0] * b[0] + b[1] * b[1]);
  result[1] = (a[1] * b[0] - a[0] * b[1]) / (b[0] * b[0] + b[1] * b[1]);
  /* endsolution */
  return;
}

void
complexProduct (double a[], double b[], double result[])
{
  /* solution */
  result[0] = a[0] * b[0] - a[1] * b[1];
  result[1] = a[0] * b[1] + a[1] * b[0];
  /* endsolution */
  return;
}

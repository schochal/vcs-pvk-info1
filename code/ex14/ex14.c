#include <iostream>

using namespace std;

double func (double x);
double rectangular_quadrature (double a, double b, int N);
double trapezoidal_quadrature (double a, double b, int N);

int
main ()
{
  int N = 500;
  cout << "correct answer: 9" << endl;
  cout << "rectangular quadrature: " << rectangular_quadrature (0, 3, N) << endl;;
  cout << "trapezoidal quadrature: " << trapezoidal_quadrature (0, 3, N) << endl;;

  return 0;
}

double
func (double x)
{
  return x * x;
}

double
rectangular_quadrature (double a, double b, int N)
{
  /* solution */
  double sum = 0;
  double h = (b - a) / N;

  for (int i = 0; i < N; i++)
  {
    sum += h * func (a + (i + .5) * h);
  }

  return sum;
  /* endsolution */
}

double
trapezoidal_quadrature (double a, double b, int N)
{
  /* solution */
  double sum = 0;
  double h = (b - a) / N;

  for (int i = 0; i < N; i++)
  {
    sum += h * (func (a + i * h) + func (a + (i + 1) * h)) / 2;
  }
  return sum;
  /* endsolution */
}

#include <iostream>

using namespace std;

double sin_sqr (double x, double eps);

int
main ()
{
  cout << "sin^2(1) = " << sin_sqr (1, 1e-5) << endl;
  return 0;
}

double
sin_sqr (double x, double eps)
{
  /* solution */
  double summand = x * x;
  double sum = summand;
  int n = 3;
  while (summand * summand > eps * eps) {
    double summand_new = -summand * 2 * 2 * x * x / (n * (n + 1));
    summand = summand_new;
    sum += summand;
    n += 2;
  }
  return sum;
  /* endsolution */
}

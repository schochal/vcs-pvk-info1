#include <iostream>
#include <cmath>

using namespace std;

double LennardJonesEnergy (double x[], double y[], double z[],
double alpha[], double beta[], int N);

int main() {
  const int N = 10;
  double x[N];
  double y[N];
  double z[N];
  double alpha[N];
  double beta[N];

  for(int i = 0; i < N; i++) {
    double r = ((double)rand() / (double)(RAND_MAX));
    x[i] = r;
    r = ((double)rand() / (double)(RAND_MAX));
    y[i] = r;
    r = ((double)rand() / (double)(RAND_MAX));
    z[i] = r;
    r = ((double)rand() / (double)(RAND_MAX));
    alpha[i] = r;
    r = ((double)rand() / (double)(RAND_MAX));
    beta[i] = r;
  }  

  cout << LennardJonesEnergy(x, y, z, alpha, beta, N) << endl;
  return 0;
}

double LennardJonesEnergy (double x[], double y[], double z[],
                           double alpha[], double beta[], int N) {
  /* solution */
  double ene = 0.0;
  double r2, r6;
  for (int i = 0; i < N-1; i++) {
    for (int j = i+1; j < N; j++) {
      r2 = (x[j] - x[i]) * (x[j] - x[i])
      + (y[j] - y[i]) * (y[j] - y[i])
      + (z[j] - z[i]) * (z[j] - z[i]);
      r6 = r2*r2*r2;
      ene += ( - alpha[i] * alpha[j] + beta[i] * beta[j] / r6 ) / r6;
    }
  }
  return ene;
	/* endsolution */
}
